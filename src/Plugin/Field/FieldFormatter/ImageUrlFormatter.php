<?php

namespace Drupal\static_asset_cache_buster\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageUrlFormatter as CoreImageUrlFormatter;

/**
 * Extends core ImageUrlFormatter to append cache buster.
 */
class ImageUrlFormatter extends CoreImageUrlFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    if (empty($elements)) {
      // Early opt-out if the field is empty.
      return;
    }
    $images = $this->getEntitiesToView($items, $langcode);

    /** @var \Drupal\file\FileInterface[] $images */
    foreach ($images as $delta => $image) {
      $changed = $image->getChangedTime();
      $timestamp_query = _static_asset_cache_buster_get_cache_buster_query($changed);
      $elements[$delta]['#markup'] .= (strpos($elements[$delta]['#markup'], '?') !== FALSE ? '&' : '?') . UrlHelper::buildQuery($timestamp_query);
    }

    return $elements;
  }

}
