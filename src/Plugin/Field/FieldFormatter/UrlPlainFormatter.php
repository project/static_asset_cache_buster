<?php

namespace Drupal\static_asset_cache_buster\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\UrlPlainFormatter as CoreFieldFormatter;

/**
 * Extends core UrlPlainFormatter to append cache buster.
 */
class UrlPlainFormatter extends CoreFieldFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      $changed = $file->getChangedTime();
      $timestamp_query = _static_asset_cache_buster_get_cache_buster_query($changed);
      $elements[$delta]['#markup'] .= (strpos($elements[$delta]['#markup'], '?') !== FALSE ? '&' : '?') . UrlHelper::buildQuery($timestamp_query);
    }

    return $elements;
  }

}
