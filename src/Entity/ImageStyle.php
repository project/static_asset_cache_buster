<?php

namespace Drupal\static_asset_cache_buster\Entity;

use Drupal\Component\Utility\UrlHelper;
use Drupal\image\Entity\ImageStyle as CoreImageStyle;

/**
 * Extends core's ImageStyle entity plugin.
 */
class ImageStyle extends CoreImageStyle {

  /**
   * {@inheritdoc}
   *
   * Add timestamp query parameter to derivative images.
   */
  public function buildUrl($path, $clean_urls = NULL) {
    $file_url = parent::buildUrl($path, $clean_urls);

    // Get the file object from its path.
    /** @var \Drupal\file\FileInterface[] $files */
    $files = \Drupal::entityTypeManager()
      ->getStorage('file')
      ->loadByProperties(['uri' => $path]);
    /** @var \Drupal\file\FileInterface$file */
    $file = reset($files);

    $changed = $file->getChangedTime();
    $timestamp_query = _static_asset_cache_buster_get_cache_buster_query($changed);
    $file_url .= (strpos($file_url, '?') !== FALSE ? '&' : '?') . UrlHelper::buildQuery($timestamp_query);

    return $file_url;
  }

}
