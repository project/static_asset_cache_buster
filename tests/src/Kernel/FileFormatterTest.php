<?php

namespace Drupal\Tests\static_asset_cache_buster\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests the file field formatters.
 *
 * @group static_asset_cache_buster
 */
class FileFormatterTest extends FormatterTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    FieldStorageConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => $this->fieldName,
      'type' => 'file',
    ])->save();

    FieldConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => $this->fieldName,
      'bundle' => $this->bundle,
      'settings' => [
        'file_extensions' => 'txt',
      ],
    ])->save();
  }

  /**
   * Tests the file field formatters.
   *
   * @dataProvider fileFieldFormatterConfigurationDataProvider
   */
  public function testFieldFormatterTest($format, $settings, $output_altered) {
    $this->testFormatter($format, $settings, $output_altered);
  }

  /**
   * Data provider for testFieldFormatterTest().
   *
   * @return array
   *   Nested arrays of values:
   *     - $format (the format)
   *     - $settings (settings for the field formatter)
   *     - $output_altered (whether the rendered output should be altered)
   */
  public function fileFieldFormatterConfigurationDataProvider() {
    return [
      [
        'file_default',
        [
          'use_description_as_link_text' => TRUE,
        ],
        TRUE,
      ],
      [
        'file_table',
        [
          'use_description_as_link_text' => TRUE,
        ],
        TRUE,
      ],
      [
        'file_url_plain',
        [],
        TRUE,
      ],
    ];
  }

}
