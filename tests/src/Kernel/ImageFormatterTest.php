<?php

namespace Drupal\Tests\static_asset_cache_buster\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\image\Entity\ImageStyle;

/**
 * Tests the image and responsive image field formatters.
 *
 * @group static_asset_cache_buster
 */
class ImageFormatterTest extends FormatterTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['image', 'responsive_image'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    FieldStorageConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => $this->fieldName,
      'type' => 'image',
    ])->save();

    FieldConfig::create([
      'entity_type' => $this->entityType,
      'field_name' => $this->fieldName,
      'bundle' => $this->bundle,
      'settings' => [
        'file_extensions' => 'jpg',
      ],
    ])->save();

    ImageStyle::create(['name' => 'large'])->save();
  }

  /**
   * Tests the image and responsive image field formatters.
   *
   * @dataProvider imageFieldFormatterConfigurationDataProvider
   */
  public function testImageFormatterTest($format, $settings, $output_altered) {
    $this->testFormatter($format, $settings, $output_altered);
  }

  /**
   * Data provider for testImageFormatterTest().
   *
   * @return array
   *   Nested arrays of values:
   *     - $format (the format)
   *     - $settings (settings for the field formatter)
   *     - $output_altered (whether the rendered output should be altered)
   */
  public function imageFieldFormatterConfigurationDataProvider() {
    return [
      [
        'image',
        [
          'image_style' => '',
        ],
        TRUE,
      ],
      [
        'image',
        [
          'image_style' => 'large',
        ],
        TRUE,
      ],
      [
        'responsive_image',
        [
          'responsive_image_style' => 'narrow',
        ],
        TRUE,
      ],
      [
        'image_url',
        [
          'image_style' => '',
        ],
        TRUE,
      ],
      [
        'image_url',
        [
          'image_style' => 'large',
        ],
        TRUE,
      ],
    ];
  }

}
