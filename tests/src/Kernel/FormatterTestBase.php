<?php

namespace Drupal\Tests\static_asset_cache_buster\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\file\Entity\File;
use Drupal\Tests\field\Kernel\FieldKernelTestBase;

/**
 * Base test class for formatter tests.
 *
 * @group static_asset_cache_buster
 */
abstract class FormatterTestBase extends FieldKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['file', 'static_asset_cache_buster'];

  /**
   * The entity type.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The bundle.
   *
   * @var string
   */
  protected $bundle;

  /**
   * The field name.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);

    $this->entityType = 'entity_test';
    $this->bundle = $this->entityType;
    $this->fieldName = mb_strtolower($this->randomMachineName());
  }

  /**
   * Helper function to test formatters.
   *
   * @param string $format
   *   The format.
   * @param array $settings
   *   Settings for the field formatter.
   * @param bool $output_altered
   *   Whether the rendered output should be altered.
   */
  protected function testFormatter(string $format, array $settings, bool $output_altered) {
    $display = \Drupal::service('entity_display.repository')
      ->getViewDisplay($this->entityType, $this->bundle)
      ->setComponent($this->fieldName, [
        'type' => $format,
        'label' => 'hidden',
        'settings' => $settings,
      ]);
    $display->save();

    // Create a test entity with a file entity attached.
    $entity = EntityTest::create([
      'name' => $this->randomMachineName(),
    ]);
    $entity->{$this->fieldName}->generateSampleItems(1);
    $entity->save();

    $build = $display->build($entity);

    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = $this->container->get('renderer');
    $output = $renderer->renderRoot($build[$this->fieldName][0]);

    $file_id = $entity->{$this->fieldName}->__get('target_id');

    /** @var \Drupal\file\Entity\FileInterface */
    $file = File::load($file_id);
    $cache_buster_string = _static_asset_cache_buster_get_cache_buster_query($file->getChangedTime());

    if ($output_altered) {
      $this->assertStringContainsString('cb=' . $cache_buster_string['cb'], (string) $output);
    }
    else {
      $this->assertStringNotContainsString('cb=' . $cache_buster_string['cb'], (string) $output);
    }
  }

}
