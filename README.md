# Static Asset Cache Buster

When static assets, such as images and PDFs, are served by the web server, they
are cached by external caches (e.g. Varnish, CDN, browsers). This becomes
problematic when a given asset is replaced on the web server.

For example, if https://example.com/sites/default/files/2022-annual-report.pdf
is updated with a newer file with the same file name, the external caches will
continue to serve the old version. This issue exists when using modules like
[Media Entity File Replace](https://www.drupal.org/project/media_entity_file_replace) and [Media: Acquia DAM](https://www.drupal.org/project/media_acquiadam).

This module seeks to address the issue by appending a query string to asset
URLs, which acts as a cache buster. The value of the 'cb' query parameter is
a hashed/truncated value from the file entity's 'changed' timestamp.

For example, *2022-annual-report.pdf* becomes
*2022-annual-report.pdf?cb=64227fd1*. When the file is updated, the value of
the 'cb' query string parameter will change.

This module doesn't provide any configuration.

## Supported Uses
This module supports the following use cases:

- The 'image' and 'responsive_image' theme hooks
  - This includes the 'Image' (image), 'Responsive Image' (responsive_image),
    and 'URL to image' (image_url) field formatters.
  - Image derivative URLs also also also included.
- The 'file_link' them hook
  - This includes the the 'Generic file' (file_default),
    'Table of Files' (file_table), and 'URL to file' (file_url_plain)
    field formatters.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

This module provides no configuration. When this module is enabled, all
functionality is enabled.

## Maintainers

- Chris Burge - [chris burge](https://www.drupal.org/u/chris-burge).
